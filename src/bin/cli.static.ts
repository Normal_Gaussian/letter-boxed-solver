import * as fs from 'fs';
import { assert } from 'console';
import { Writable } from 'stream';

const log = {
    debug: (...args: unknown[]) => console.log(`[DEBUG]`, ...args),
    info: (...args: unknown[]) => console.log(`[ INFO]`, ...args),
    warn: (...args: unknown[]) => console.log(`[ WARN]`, ...args),
    error: (...args: unknown[]) => console.log(`[ERROR]`, ...args),
};

const locale = (process.env.LANG || process.env.LANGUAGE || process.env.LC_ALL || process.env.LC_MESSAGES || 'en_US.UTF-8').split('.')[0]

class ArgumentError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'ArgumentError';
    }
}

interface Args {
    dictionary: string[],
    output: Writable,
    edges: string[]
};

async function parseArgs(args: string[]): Promise<Partial<Args>> {
    
    const argParsingFunctions: {[k: string]: undefined | ((nextArg: () => string | undefined, peekArg: () => string | undefined) => Promise<Partial<Args>>) } = {
        // Dictionary file as a list of string
        '--dictionary': async (nextArg: () => string | undefined): Promise<Partial<Args>> => {
            const file = nextArg();
            if(!file) {
                throw new ArgumentError(`Missing value for argument '--dictionary'`);
            }
            if(!fs.existsSync(file)) {
                throw new ArgumentError(`No file matching '--dictionary' argument "${file}"`)
            }
            const data = await fs.promises.readFile(file, { encoding: 'utf-8'});
            const words = data.split(/\r?\n/g).map(line => line.trim().toLocaleLowerCase(locale)).filter(line => line.length > 0);

            return {
                dictionary: words
            }
        },

        // Output file as a stream writer
        '--output': async (nextArg: () => string | undefined): Promise<Partial<Args>> => {
            const file = nextArg();
            if(!file) {
                throw new ArgumentError(`Missing value for argument '--output'`);
            }
            if(fs.existsSync(file)) {
                log.warn(`output file already exists "${file}"`);
            }
            const writer = fs.createWriteStream(file, { encoding: 'utf-8'});

            return {
                output: writer
            };
        },


    };

    async function _default(nextArg: () => string | undefined, peekArg: () => string | undefined): Promise<Partial<Args>> {
        const edges: string[] = [];
        while(!argParsingFunctions[peekArg() ?? '']) {
            const edge = nextArg();
            if(!edge) {
                break;
            }
            edges.push(edge);
        }
        return { edges }
    }

    let i=0;
    function nextArg() {
        i+=1;
        return args[i];
    }
    function peekArg() {
        return args[i+1];
    }
    let result: Partial<Args> = {};
    for(; i<args.length; i++) {
        const argFunc = argParsingFunctions[args[i]];
        if(argFunc) {
            Object.assign(result, await argFunc(nextArg, peekArg));
        } else {
            Object.assign(result, await _default(nextArg, peekArg));
        }
    }
    return result;
}

// Parse the CLI

// solve [--dictionary <dictionary file>] [--output <output file>] [<edge 1> <edge 2> <edge 3> <edge 4>]

async function getDefaultDictionary() {
    // If we are on linux, try and get the amercian-english dictionary
    // If that isn't there, get the system dictionary
    // TODO: If that isn't there use a fallback
    const americanEnglish = '/usr/share/dict/american-english';
    const words = '/usr/share/dict/words';

    let dict: string | null = null;
    if(fs.existsSync(americanEnglish)) {
        dict = americanEnglish;
        log.info(`No dictionary provided - falling back to american english (${americanEnglish})`);
    } else if(fs.existsSync(words)) {
        dict = words;
        log.info(`No dictionary provided - falling back to system dictionary (${words})`);
    }

    if(!dict) {
        throw new Error(`Couldn't find a suitable dictionary`);
    }

    return (await fs.promises.readFile(dict, {encoding: 'utf-8'})).split('\n').filter(i => i);
}

class Puzzle {
    edges: string[];
    
    // Helpers
    edgeCount: number;
    letters_set: Set<string>;
    letter_counts: {[character: string]: undefined | number} = {};
    letters_string: string;

    // Warnings
    duplicate_letters: boolean;
    uneven_edges: boolean;
    non_standard_characters: boolean;
    not_a_box: boolean;
    non_standard_edge_length: boolean;

    constructor(edges: string[]) {
        this.edges = edges;

        this.edgeCount = edges.length;
        this.letters_string = edges.reduce((acc, v) => acc + v, '').split('').sort().join('');
        for(const c of this.letters_string) {
            this.letters_set.add(c);
            this.letter_counts[c] = (this.letter_counts[c] ?? 0) + 1;
        }

        this.duplicate_letters = this.letters_set.size < this.letters_string.length;
        if(this.duplicate_letters) {
            log.warn(`Puzzle contains duplicate letters`);
        }

        this.uneven_edges = this.edges.every((e, _i, a) => e.length === a[0].length);
        if(this.uneven_edges) {
            log.warn(`Edges are not all the same size`);
        }

        this.non_standard_edge_length = this.edges.some(e => e.length !== 3);
        if(this.non_standard_edge_length) {
            log.warn(`Edges are not the traditional length (3)`);
        }

        this.not_a_box = this.edges.length !== 4;
        if(this.not_a_box) {
            log.warn(`Puzzle is not a box shape`);
        }
    }

    static fromEdges(edges: string[]): Puzzle {
        return new Puzzle(edges);
    }

    static alphabets = { 
        englishLatin: 'abcdefghijklmnopqrstuvwxyz'
    };

    static generate(options?: { alphabet?: string, edgeStructure?: number[], duplicates?: boolean}): Puzzle {
        const { alphabet, edgeStructure, duplicates } = Object.assign({
            alphabet: Puzzle.alphabets.englishLatin,
            edgeStructure: [3, 3, 3, 3],
            duplicates: false
        }, options);

        const perimeter = edgeStructure.reduce((acc, v) => acc + v);
        
        if(!duplicates && alphabet.length < perimeter) {
            throw new Error(`Cannot create puzzle without duplicates when it contains more letters than are in the alphabet.`)
        }

        function choose({ alphabet, replacement, length }: { alphabet: string, replacement: boolean, length: number }): string {
            let bag = alphabet.split('');
            let choice = '';
            while(choice.length < length) {
                const position = Math.floor(Math.random() * bag.length);
                choice = choice + bag[position];
                if(!replacement) {    
                    bag.splice(position, 1)[0];
                }
            }

            return choice;
        }

        // Choose all the letters
        const letters = choose({ alphabet, replacement: false, length });
        const edges: string[] = [];
        let position = 0;
        for(let edgeLength of edgeStructure) {
            edges.push(letters.slice(position, position + edgeLength));
            position += edgeLength;
        }

        // Verify
        assert(edges.length === edgeStructure.length, `"edges" and "edgeStructure" should have the same length`);
        for(let i=0; i<edgeStructure.length; i++) {
            assert(edges[i].length === edgeStructure[i], `"edge[${i}]" has unexpected length "${edges[i].length}". Expected "${edgeStructure[i]}"`);
        }

        return new Puzzle(edges);
    }

    solve({ dictionary: rawDictionary, output }: { dictionary: string[], output: Writable }) {
        // There is definitely a cleverer solution involving a bitmask or something.

        // First, let us reduce the dictionary to only words that are possible
        const reducedDictionary = rawDictionary.filter(word => {
            // Remove words that use letters that are not in the letter box
            for(const character of word) {
                if(!this.letters_set.has(character)) {
                    return false;
                }
            }

            // Check that we have enough of the letters
            if(!this.duplicate_letters) {
                // We don't have duplicates, remove all words with duplicates
                const duplicates_in_word = word.split('').sort().filter((v, i, a) => a[i-1] === v).length > 0;
                if(duplicates_in_word) {
                    return false;
                }
            } else {
                // We do have duplicates, remove words without the right duplicates...
                const letter_counts: {[character: string]: undefined | number} = {};
                for(const letter of word.split('')) {
                    const count = (letter_counts[letter] ?? 0) + 1;
                    if((this.letter_counts[letter] ?? 0) < count) {
                        return false;
                    }
                    letter_counts[letter] = count;
                }
            }
            return true;
        }).sort();

        // Get some bookmarks based on starting letter
        const startingLetters: {[letter: string]: {start: number, length: number, offset: number }} = {};
        for(const letter in this.letters_set) {
            const start = reducedDictionary.findIndex(v => v[0] === letter);
            let length = 0;
            for(; start + length < reducedDictionary.length; length++) {
                if(reducedDictionary[start + length][0] !== letter) {
                    break;
                }
            }
            startingLetters[letter] = {
                start,
                offset: 0,
                length
            };
        }

        // Ok. Now how do we construct word chains?
        // Lets do a dumb depth first search
        let chain: string[] = [];
        const toGet = {...this.letter_counts};
        for(let i=0; i<reducedDictionary.length; i++) {
            const word = reducedDictionary[i];
            chain.push(word);
            for(const character of word) {
                const count = (toGet[character] ?? 1) - 1;
                toGet[character] = count;
                if(!count) {
                    delete toGet[character];
                }
            }
            if(toGet)
        }
    }
}

async function start() {
    const args = await parseArgs(process.argv);

    const dictionary = args.dictionary ?? await getDefaultDictionary();

    const output = args.output ?? process.stdout;

    const puzzle = args.edges ? Puzzle.fromEdges(args.edges) : Puzzle.generate({ alphabet: 'abcdefghijklmnopqrstuvwxyz', edgeStructure: [3, 3, 3, 3], duplicates: false });

    const result = puzzle.solve({
        dictionary,
        output
    });
}

start();

// TODO: output result to cli ?
