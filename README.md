# Letter Boxed Solver

Solves puzzles i n the style of the New York Times Games's 'Letter-Boxed'.

## The letter boxed game

In this game there are 12 letters arranged evenly across the four edges of a 'box' such that each edge has 3 letters.

The player must create words where:
* Each successive letter is from a different edge to the letter prior to it
* Each word starts with the letter the previous word ended with

The aim of the game is to find a series of words such that all letters are used at least once, and the total number of words is fewer than a given target.

## Solving

The solver will, for a given puzzle and dictionary:

* Generate a flowchart showing some solutions.
* Provide some statistics about the number of solutions.
* Highlight notable words.
* Highlight notable solutions.

## Usage

`solve [--dictionary <dictionary file>] [--output <output file>] [<edge 1> <edge 2> <edge 3> <edge 4>]`

* `--dictionary <file>`
  The given file is to be used as the dictionary for solving the puzzle. Only words in the dictionary will be used.
  The file should be arranged so that each word is on a new line. Punctuation will be ignored.
  If no dictionary is provided, a default dictionary will be used.
* `--output <file>`
  The output will be written to the given file.
  If no file is provided the output will be written to stdout.
* `<edge 1> <edge 2> <edge 3> <edge 4>`
  The edges to use for the game.
  If no edges are provided, four edges of three letters each will be generated.
  Edges are groups of characters with no spaces. Warnings will be given if edges do not follow the format of the official game, but a solution will be generated regardless.
